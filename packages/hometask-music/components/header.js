import logo from "./../assets/icons/logo.svg";
import { createElement } from "./createElement";
let headerLinks;

const app = document.getElementById("wrapper");
const header = createElement({
  el: "header",
  parent: app,
  className: "header",
});
const nav = createElement({
  el: "nav",
  parent: header,
  className: "header__nav",
});
const logoIcon = createElement({
  el: "img",
  parent: nav,
  className: "header__logo",
});
logoIcon.setAttribute("src", logo);
headerLinks = createElement({
  el: "ul",
  parent: nav,
  className: "header__nav__menu",
});
const navTitles = {
  simo: "",
  discover: "/landing-page",
  join: "/sign-up",
  "sign in": "",
};
for (const [title, href] of Object.entries(navTitles)) {
  if (!href) {
    const el = createElement({ el: "li", parent: headerLinks });
    el.innerText = title;
    if (title === "simo") {
      el.classList.add("header__nav__logo-title");
    }
    continue;
  }
  const el = createElement({ el: "li", parent: headerLinks });
  const link = createElement({ el: "a", parent: el });
  link.setAttribute("href", href);
  link.innerText = title;
}

export { headerLinks };
