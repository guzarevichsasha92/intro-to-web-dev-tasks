import { mainContent } from "./home";
import { signUpWrapper } from "./join";
import { featureWrapper } from "./discover";
import { headerLinks } from "./header";

export const renderer = {
  "/": mainContent,
  "/landing-page": featureWrapper,
  "/sign-up": signUpWrapper,
};
export const linksList = headerLinks.children;
