function createElement({ el, parent, className }) {
  const element = document.createElement(el.toString());
  if (className) {
    element.classList.add(className);
  }
  if (parent) {
    parent.appendChild(element);
  }
  return element;
}

export { createElement };
