import { createElement } from "./createElement";

const app = document.getElementById("wrapper");
export const main = createElement({
  el: "main",
  parent: app,
  className: "main",
});

const copyRight = createElement({
  el: "p",
  parent: main,
  className: "main__copyright",
});

copyRight.innerText = "Wlodzimierz Simon © 2022";
