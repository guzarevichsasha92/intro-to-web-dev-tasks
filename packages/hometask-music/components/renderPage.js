import { main } from "./main";
import { renderer } from "./renderingLinks";
import { linksList } from "./renderingLinks";

window.addEventListener("popstate", () => {
  for (let i = 1; i < linksList.length; i++) {
    if (linksList[i].children[0]) {
      linksList[i].style.display = "block";
    }
  }
  let link = location.pathname.split("/");
  const href = "/" + link.slice(link.length - 1, link.length).join("/");
  main.replaceChildren(renderer[href]);
  for (let i = 0; i < linksList.length; i++) {
    if (
      linksList[i].children[0] &&
      linksList[i].children[0].pathname === href
    ) {
      linksList[i].style.display = "none";
    }
  }
});
