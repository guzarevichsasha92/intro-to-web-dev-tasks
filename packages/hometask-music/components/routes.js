import { headerLinks } from "./header";
import { main } from "./main";
import { renderer } from "./renderingLinks";
import { linksList } from "./renderingLinks";

export function routes() {
  headerLinks.addEventListener("click", (e) => {
    e.preventDefault();
    if (e.target.href) {
      for (let i = 1; i < linksList.length; i++) {
        if (linksList[i].children[0]) {
          linksList[i].style.display = "block";
        }
      }
      let link = location.pathname.split("/");
      const routes = ["sign-up", "landing-page"];
      link.forEach((path, index) => {
        if (routes.includes(path)) {
          link = link.slice(0, index).concat(link.slice(index + 1));
        }
      });

      let tail = link.join("/");
      if (tail.endsWith("/")) {
        tail = tail.slice(0, tail.length - 1);
      }

      history.pushState({}, "", tail + e.target.pathname);

      main.replaceChildren(renderer[e.target.pathname]);
      e.target.parentElement.style.display = "none";
    }
  });
}
