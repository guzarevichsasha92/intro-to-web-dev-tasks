import { main } from "./main";
import { linksList, renderer } from "./renderingLinks";
import { href } from "./url";

window.addEventListener("load", () => {
  for (let i = 0; i < linksList.length; i++) {
    if (
      linksList[i].children[0] &&
      linksList[i].children[0].pathname === href
    ) {
      linksList[i].style.display = "none";
    }
  }

  main.replaceChildren(renderer[href]);
});
