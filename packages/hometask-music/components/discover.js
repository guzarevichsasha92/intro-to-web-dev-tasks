import { createElement } from "./createElement";
import { main } from "./main";
import image from "./../assets/images/music-titles.png";

export const featureWrapper = document.createElement("div");
featureWrapper.classList.add("main__feature-page");

const title = createElement({
  el: "h1",
  parent: featureWrapper,
  className: "main__feature-page__title",
});
title.innerText = "Discover new music";

const buttonBlock = createElement({
  el: "div",
  parent: featureWrapper,
  className: "main__feature-page__button-block",
});

const buttonNames = ["Charts", "Songs", "Artists"];

buttonNames.forEach((button) => {
  const el = createElement({
    el: "button",
    parent: buttonBlock,
    className: "button",
  });
  el.innerText = button;
});

const subtitle = createElement({
  el: "h3",
  parent: featureWrapper,
  className: "main__feature-page__subtitle",
});

subtitle.innerText =
  "By joing you can benefit by listening to the latest albums released";

const picture = createElement({
  el: "img",
  parent: featureWrapper,
  className: "main__feature-page__image",
});
// picture.classList.add("bg-image");
picture.setAttribute("src", image);
