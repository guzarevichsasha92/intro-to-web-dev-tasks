import { createElement } from "./createElement";

const app = document.getElementById("wrapper");
const footer = createElement({
  el: "footer",
  parent: app,
  className: "footer",
});
const footerMenu = createElement({
  el: "ul",
  parent: footer,
  className: "footer__menu",
});
const footerTitles = ["about us", "contacts", "CR info", "twitter", "facebook"];
footerTitles.forEach((title) => {
  const el = createElement({ el: "li", parent: footerMenu });
  const link = createElement({ el: "a", parent: el });
  link.setAttribute("href", "#");
  link.innerText = title;
});
