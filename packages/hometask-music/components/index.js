import "./header";
import "./home";
import "./discover";
import "./join";
import "./footer";
import { routes } from "./routes";
import "./renderPage";
import "./pageRefresh";

routes();
