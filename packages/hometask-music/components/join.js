import { createElement } from "./createElement";
import image from "./../assets/images/background-page-sign-up.png";

export const signUpWrapper = document.createElement("div");
signUpWrapper.classList.add("main__sign-up-wrapper");

const form = {
  "Name:": "username",
  "Password:": "password",
  "e-mail:": "email",
};

for (const [label, tagName] of Object.entries(form)) {
  const el = createElement({ el: "div", parent: signUpWrapper });
  const labelEl = createElement({ el: "label", parent: el });
  const labelElText = createElement({ el: "span", parent: labelEl });
  labelElText.innerText = label;
  const input = createElement({ el: "input", parent: labelEl });
  input.type = tagName;
  input.name = tagName;
}

const picture = createElement({
  el: "img",
  parent: signUpWrapper,
  className: "main__sign-up-wrapper__image",
});

picture.setAttribute("src", image);
picture.classList.add("bg-image");
