import { createElement } from "./createElement";
import { main } from "./main";
import image from "./../assets/images/background-page-landing.png";

export const mainContent = createElement({
  el: "div",
  parent: main,
  className: "main__home__content",
});
const title = createElement({
  el: "h1",
  parent: mainContent,
  className: "main__home__content__title",
});
title.innerText = "Feel the music";
const subtitle = createElement({
  el: "h3",
  parent: mainContent,
  className: "main__home__content__subtitle",
});
subtitle.innerText = "Stream over 10 million songs with one click";
const joinButton = createElement({
  el: "button",
  parent: mainContent,
  className: "button",
});
joinButton.innerText = "Join now";
const picture = createElement({
  el: "img",
  parent: mainContent,
  className: "main__home__image",
});
picture.classList.add("bg-image");
picture.setAttribute("src", image);
