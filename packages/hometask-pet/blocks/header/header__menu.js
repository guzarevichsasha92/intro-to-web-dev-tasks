import cross from "../../assets/icons/x-lg.svg";
import menu from "../../assets/icons/menu.svg";

const mainMenu = document.getElementById("main-menu");
const button = document.getElementById("toggle-menu");
const buttonIcon = document.getElementById("button-icon");
const menuDropdown = document.getElementById("menu-dropdown");
const navMenu = document.getElementById("nav-menu");
button.addEventListener("click", (event) => {
  event.preventDefault();
  mainMenu.classList.toggle("is-open");
  if (Array.from(mainMenu.classList).includes("is-open")) {
    buttonIcon.setAttribute("src", cross);
    menuDropdown.insertBefore(button, navMenu);
  } else {
    buttonIcon.setAttribute("src", menu);
    mainMenu.appendChild(button);
  }
});
