import cross from "../assets/icons/cross.svg";
import menuIcon from "../assets/icons/menu.svg";

const menuButton = document.getElementById("burger-menu-button");
const menu = document.getElementById("burger-menu");

menuButton.addEventListener("click", (e) => {
  e.preventDefault();
  menu.classList.toggle("is-open");
  if (menu.classList.contains("is-open")) {
    menuButton.setAttribute("src", cross);
  } else {
    menuButton.setAttribute("src", menuIcon);
  }
});
