import Swiper, { Navigation } from "swiper";
// import Swiper styles
import "swiper/css";
import "swiper/css/navigation";

const swiper = new Swiper(".swiper", {
  // If we need pagination
  loop: true,
  modules: [Navigation],
  breakpoints: {
    768: {
      slidesPerView: 3,
    },
  },
  // Navigation arrows
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
});
